# Copyright © 2024 Neven Sajko. All rights reserved.
#
# Licensed under the MIT license, see the LICENSE file.

module StaticViews

using TypeCompletion: TypeCompletion

export StaticView

function check_static_parameters_against_iterator_length(::Val{O}, ::Val{L}, p) where {O,L}
  par_len = length(p)
  if par_len < O
    throw(ArgumentError("skip count greater than parent length"))
  end
  if par_len < L
    throw(ArgumentError("view length greater than parent length"))
  end
end

function check_arguments(o::Val, l::Val, p)
  if Base.IteratorSize(p) isa Union{Base.HasLength,Base.HasShape}
    check_static_parameters_against_iterator_length(o, l, p)
  end
end

"""
    StaticView{SkipCount,Length,Parent,SomeAdditionalRedundantParameters...}(parent)

Constructs a value representing a *view* of `parent` not including the first
`SkipCount` elements of `parent`, and only including `Length` elements of `parent`.

Both `SkipCount` and `Length` are nonnegative `Int` values. This is enforced using
the redundant parameters. The TypeCompletion.jl interface is implemented to help with
the redundant parameters.

Assumes `parent` supports these functions from `Base`:
* `getindex`
* `firstindex`

Supports these functions from `Base`:
* `getindex`
* `firstindex`, implemented as `firstindex(parent)`
* `lastindex`
* `first`
* `last`
* `front`
* `tail`
* `length`
* `isempty`
"""
struct StaticView{SkipCount,Length,Parent,OT<:NTuple{SkipCount,Nothing},LT<:NTuple{Length,Nothing}}
  p::Parent
  function StaticView{O,L,P,OT,LT}(p::P) where {O,L,P,OT<:NTuple{O,Nothing},LT<:NTuple{L,Nothing}}
    check_arguments(Val(O), Val(L), p)
    new{O,L,P,OT,LT}(p)
  end
end

const nonredundant = StaticView{O,L,P,NTuple{O,Nothing},NTuple{L,Nothing}} where {O,L,P}

# 0
TypeCompletion.complete_overload(::Type{StaticView               })               = nonredundant
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,P}}) where {P    } = nonredundant{<:Any,<:Any,P}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L      }}) where {L    } = nonredundant{<:Any,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,P    }}) where {L,P  } = nonredundant{<:Any,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O            }}) where {O    } = nonredundant{O}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,P    }}) where {O,P  } = nonredundant{O,<:Any,P}
TypeCompletion.complete_overload(::Type{StaticView{O,L          }}) where {O,L  } = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,L,P        }}) where {O,L,P} = nonredundant{O,L,P}
# 1
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,<:Any,<:Any,NTuple{L,Nothing}}}) where {L} = nonredundant{<:Any,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,P,<:Any,NTuple{L,Nothing}}}) where {P,L} = nonredundant{<:Any,L,P}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,<:Any,<:Any,NTuple{L,Nothing}}}) where {L} = nonredundant{<:Any,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,P,<:Any,NTuple{L,Nothing}}}) where {L,P} = nonredundant{<:Any,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,<:Any,<:Any,NTuple{L,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,P,<:Any,NTuple{L,Nothing}}}) where {O,P,L} = nonredundant{O,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O,L,<:Any,<:Any,NTuple{L,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,L,P,<:Any,NTuple{L,Nothing}}}) where {O,L,P} = nonredundant{O,L,P}
# 2
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,<:Any,NTuple{O,Nothing}}}) where {O} = nonredundant{O}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,P,NTuple{O,Nothing}}}) where {P,O} = nonredundant{O,<:Any,P}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,<:Any,NTuple{O,Nothing}}}) where {L,O} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,P,NTuple{O,Nothing}}}) where {L,P,O} = nonredundant{O,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,<:Any,NTuple{O,Nothing}}}) where {O} = nonredundant{O}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,P,NTuple{O,Nothing}}}) where {O,P} = nonredundant{O,<:Any,P}
TypeCompletion.complete_overload(::Type{StaticView{O,L,<:Any,NTuple{O,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,L,P,NTuple{O,Nothing}}}) where {O,L,P} = nonredundant{O,L,P}
# 3
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,<:Any,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,<:Any,P,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {P,O,L} = nonredundant{O,L,P}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,<:Any,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {L,O} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{<:Any,L,P,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {L,P,O} = nonredundant{O,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,<:Any,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,<:Any,P,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {O,P,L} = nonredundant{O,L,P}
TypeCompletion.complete_overload(::Type{StaticView{O,L,<:Any,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {O,L} = nonredundant{O,L}
TypeCompletion.complete_overload(::Type{StaticView{O,L,P,NTuple{O,Nothing},NTuple{L,Nothing}}}) where {O,L,P} = nonredundant{O,L,P}

function StaticView{O,L,P}(p::P) where {O,L,P}
  TypeCompletion.complete(StaticView{O,L,P})(p)
end

"""
    StaticView{SkipCount,Length}(parent)

Constructs a `StaticView{SkipCount,Length,typeof(parent)}`.
"""
function StaticView{O,L}(p::P) where {O,L,P}
  TypeCompletion.complete(StaticView{O,L,P})(p)
end

"""
    StaticView{SkipCount}(parent)

Constructs a `StaticView{SkipCount,Int(length(parent))-SkipCount,typeof(parent)}`.

Assumes `parent` supports `Base.length`.
"""
function StaticView{O}(p::P) where {O,P}
  l = Int(length(p))::Int - O
  TypeCompletion.complete(StaticView{O,l,P})(p)
end

"""
    StaticView(parent)

Constructs a `StaticView{0,Int(length(parent)),typeof(parent)}`.

Assumes `parent` supports `Base.length`.
"""
function StaticView(p::P) where {P}
  l = Int(length(p))::Int
  TypeCompletion.complete(StaticView{0,l,P})(p)
end

function Base.eltype(::Type{<:StaticView{<:Any,<:Any,P}}) where {P}
  eltype(P)
end

Base.@propagate_inbounds function Base.getindex(v::StaticView{O}, i) where {O}
  (v.p)[O + i]
end

function Base.firstindex(v::StaticView)
  firstindex(v.p)
end

function Base.lastindex(v::StaticView)
  firstindex(v) + length(v) - true
end

Base.@propagate_inbounds function Base.first(v::StaticView)
  v[firstindex(v)]
end

Base.@propagate_inbounds function Base.last(v::StaticView)
  v[lastindex(v)]
end

const SV = StaticView{<:Any,<:Any,P,OT,LT} where {OT,LT,P}

Base.front(v::SV{OT,LT}) where {O,L,OT<:NTuple{O,Nothing},LT<:Tuple{Nothing,Vararg{Nothing,L}}} = StaticView{O     ,L}(v.p)
Base.tail( v::SV{OT,LT}) where {O,L,OT<:NTuple{O,Nothing},LT<:Tuple{Nothing,Vararg{Nothing,L}}} = StaticView{O+true,L}(v.p)

function Base.length(v::StaticView{<:Any,L}) where {L}
  L
end

function Base.isempty(v::StaticView)
  iszero(length(v))::Bool
end

end
