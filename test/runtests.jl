using StaticViews
using Test
using Aqua: Aqua

const TypeCompletion = StaticViews.TypeCompletion

@testset "StaticViews.jl" begin
  @testset "Code quality (Aqua.jl)" begin
    Aqua.test_all(StaticViews)
  end

  @testset "empty" begin
    for it ∈ ((), [])
      v = StaticView{0}(it)
      @test isempty(v)
      @test_throws Exception Base.front(v)
      @test_throws Exception Base.tail(v)
    end
  end

  @testset "TypeCompletion.jl" begin
    O = 1
    L = 2
    P = NTuple{7,Int}
    OT = NTuple{O,Nothing}
    LT = NTuple{L,Nothing}
    for T ∈ (
      # 0
      StaticView, StaticView{<:Any,<:Any,P},
      StaticView{<:Any,L}, StaticView{<:Any,L,P},
      StaticView{O}, StaticView{O,<:Any,P},
      StaticView{O,L}, StaticView{O,L,P},
      # 1
      StaticView{<:Any,<:Any,<:Any,<:Any,LT}, StaticView{<:Any,<:Any,P,<:Any,LT},
      StaticView{<:Any,L,<:Any,<:Any,LT}, StaticView{<:Any,L,P,<:Any,LT},
      StaticView{O,<:Any,<:Any,<:Any,LT}, StaticView{O,<:Any,P,<:Any,LT},
      StaticView{O,L,<:Any,<:Any,LT}, StaticView{O,L,P,<:Any,LT},
      # 2
      StaticView{<:Any,<:Any,<:Any,OT}, StaticView{<:Any,<:Any,P,OT},
      StaticView{<:Any,L,<:Any,OT}, StaticView{<:Any,L,P,OT},
      StaticView{O,<:Any,<:Any,OT}, StaticView{O,<:Any,P,OT},
      StaticView{O,L,<:Any,OT}, StaticView{O,L,P,OT},
      # 3
      StaticView{<:Any,<:Any,<:Any,OT,LT}, StaticView{<:Any,<:Any,P,OT,LT},
      StaticView{<:Any,L,<:Any,OT,LT}, StaticView{<:Any,L,P,OT,LT},
      StaticView{O,<:Any,<:Any,OT,LT}, StaticView{O,<:Any,P,OT,LT},
      StaticView{O,L,<:Any,OT,LT}, StaticView{O,L,P,OT,LT},
    )
      @test TypeCompletion.complete(T) isa Type{<:StaticView}
    end
  end

  for it ∈ ((10, 20), [10, 20])
    @test eltype(it) == eltype(StaticView(it))
  end
  for iterator ∈ (3:20, ntuple((i -> i + 2), 15))
    for it ∈ (iterator, map(BigInt, iterator))
      T = StaticView{0,Int(length(it)),typeof(it)}
      @test StaticView(it) isa T
      @test StaticView{0}(it) isa T
      @test StaticView{0,Int(length(it))}(it) isa T
      @test StaticView{0,Int(length(it)),typeof(it)}(it) isa T
      for o ∈ 0:5
        v = StaticView{o}(it)
        @test !isempty(v)
        @test firstindex(v) == firstindex(it)
        @test first(v) == v[firstindex(v)] == first(it) + o
        @test first(v) == first(Base.front(v))
        @test last(v) == last(Base.tail(v))
        @test_throws ArgumentError StaticView{100,0}(it)
        @test_throws ArgumentError StaticView{100,1}(it)
        @test_throws ArgumentError StaticView{0,100}(it)
        @test_throws ArgumentError StaticView{1,100}(it)
        @test_throws Exception StaticView{-1}(it)
        @test_throws Exception StaticView{0,-1}(it)
      end
    end
  end
end
