# StaticViews

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/S/StaticViews.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/S/StaticViews.html)
[![Aqua](https://raw.githubusercontent.com/JuliaTesting/Aqua.jl/master/badge.svg)](https://github.com/JuliaTesting/Aqua.jl)

A *static view* Julia package. The "view" basically consists of an offset and a length, both in the
type domain, and of a parent container. The new type implements several functions from `Base`, such
as `getindex`, `first`, `last`, `front` and `tail`.

The motivation for the package is allowing recursive type-stable manipulations of large tuples to
be friendly for both the compiler and the programmer. Basically, instead of repeatedly applying
`tail` or `front` to tuples, construct a `StaticView` from your tuple, and apply `tail` and/or
`front` to the `StaticView`. This could help decrease compilation time while keeping optimal run
time.
